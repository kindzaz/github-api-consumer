package eu.girdziusai.githubapiconsumer.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RepositoriesController.class)
public class RepositoriesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void index_whenCallRoot_thenReturns200() throws Exception {
        mockMvc.perform(get("/")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("github-api-consumer.")));
    }

    @Test
    public void index_whenGet0JavaFrameworks_thenErrorMessage() throws Exception {
        mockMvc.perform(get("/java-frameworks?n={itemsQuantity}", 0)
                        .contentType("application/json"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("must be greater than or equal to 1")));
    }

    @Test
    public void index_whenGet101JavaFrameworks_thenErrorMessage() throws Exception {
        mockMvc.perform(get("/java-frameworks?n={itemsQuantity}", 101)
                        .contentType("application/json"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("must be less than or equal to 100")));
    }

    @Test
    public void index_whenGet2JavaFrameworks_thenShouldReturn2Items() throws Exception {
        mockMvc.perform(get("/java-frameworks?n={itemsQuantity}", 2)
                        .contentType("application/json"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*.*.name", hasSize(2)));
    }
}