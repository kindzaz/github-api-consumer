package eu.girdziusai.githubapiconsumer;

import eu.girdziusai.githubapiconsumer.dto.Item;
import eu.girdziusai.githubapiconsumer.dto.License;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    @Test
    public void groupItems_whenThreeItems_thenThreeItemsGroupedByLicenseKeyAndSortedByStarCount() {
        final Map<String, Queue<Item>> sortedItems = Utils.groupItems(getGithubRawData());
        assertEquals(2, sortedItems.size());
        assertEquals(1, sortedItems.get("licenseKeyB").size());
        assertEquals(3, sortedItems.get("licenseKeyA").size());
        assertEquals(10, sortedItems.get("licenseKeyA").poll().getStarCount());
        assertEquals(1, sortedItems.get("licenseKeyA").poll().getStarCount());
        assertEquals(1, sortedItems.get("licenseKeyA").poll().getStarCount());
    }

    private static List<Item> getGithubRawData() {
        final List<Item> items = new ArrayList<>(3);
        items.add(getItem(0, "licenseKeyA", 10));
        items.add(getItem(1, "licenseKeyB", 5));
        items.add(getItem(2, "licenseKeyA", 1));
        items.add(getItem(3, "licenseKeyA", 1));
        return items;
    }

    private static Item getItem(final int id, final String licenseKey, final int starCount) {
        final Item item = new Item();
        item.setDescription("Item" + id + "Description");
        item.setName("Item" + id + "Name");
        item.setUrl("Item" + id + "Url");
        item.setLicense(new License(licenseKey, "LicenseName"));
        item.setStarCount(starCount);
        return item;
    }
}