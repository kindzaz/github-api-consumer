package eu.girdziusai.githubapiconsumer.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item implements Comparable<Item> {

    private String name;
    private String description;
    private String url;
    private License license;
    private Integer starCount;

    @JsonIgnore
    public License getLicense() {
        return license;
    }

    @JsonProperty
    public void setLicense(License license) {
        this.license = license;
    }

    @JsonProperty("starCount")
    public Integer getStarCount() {
        return starCount;
    }

    @JsonProperty("stargazers_count")
    public void setStarCount(Integer starCount) {
        this.starCount = starCount;
    }

    @Override
    public int compareTo(Item item) {
        return item.starCount - starCount;
    }
}
