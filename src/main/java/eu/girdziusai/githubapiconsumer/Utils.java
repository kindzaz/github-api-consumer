package eu.girdziusai.githubapiconsumer;

import eu.girdziusai.githubapiconsumer.dto.Item;
import eu.girdziusai.githubapiconsumer.dto.License;

import java.util.*;

public class Utils {
	private Utils() {
	}

	public static Map<String, Queue<Item>> groupItems(final List<Item> items) {
		Map<String, Queue<Item>> groupedItems = new HashMap<>();
		for (Item item : items) {
			addItem(groupedItems, item);
		}

		return groupedItems;
	}

	private static void addItem(final Map<String, Queue<Item>> groupedItems, final Item item) {
		String licenseKey = getLicenseKey(item.getLicense());
		if (!groupedItems.containsKey(licenseKey)) {
			groupedItems.put(licenseKey, new PriorityQueue<>());
		}

		Queue<Item> itemsGroup = groupedItems.get(licenseKey);
		itemsGroup.add(item);
	}

	private static String getLicenseKey(final License license) {
		if (license == null || license.getKey() == null) {
			return "NULL";
		}

		return license.getKey();
	}
}
