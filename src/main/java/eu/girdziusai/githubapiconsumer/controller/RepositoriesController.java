package eu.girdziusai.githubapiconsumer.controller;

import eu.girdziusai.githubapiconsumer.Utils;
import eu.girdziusai.githubapiconsumer.dto.Item;
import eu.girdziusai.githubapiconsumer.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

@RestController
@Validated
public class RepositoriesController {
	public static final String LIST_JAVA_FRAMEWORKS_URL = "https://api.github.com/search/repositories?q=topic:java-frameworks&per_page={itemsQuantity}&sort=stars&order=desc";

	@Autowired
	private final RestTemplate restTemplate;
	
	@Autowired
	public RepositoriesController(final RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@GetMapping("/")
	public String index() {
		return "github-api-consumer.";
	}

	@GetMapping("/java-frameworks/raw")
	public List<Item> listJavaFrameworksRaw() {
		Response response = restTemplate.getForObject(LIST_JAVA_FRAMEWORKS_URL, Response.class, 100);
		return response.getItems();
	}

	@GetMapping("/java-frameworks")
	public Map<String, Queue<Item>> listJavaFrameworks(@RequestParam @Min(1) @Max(100) long n) {
		Response response = restTemplate.getForObject(LIST_JAVA_FRAMEWORKS_URL, Response.class, n);
		return Utils.groupItems(response.getItems());
	}
}
