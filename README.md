## Used documentation
 * https://docs.github.com/en/rest/reference/search#search-repositories  
 * https://spring.io/guides/gs/consuming-rest/
 * https://stackoverflow.com/questions/20705377/resttemplate-urivariables-not-expanded
 * https://www.tutorialspoint.com/spring_boot/spring_boot_application_properties.htm
 * https://stackoverflow.com/questions/12505141/only-using-jsonignore-during-serialization-but-not-deserialization
 * https://jsonpath.com/
 * and more


## Build, run tests and start application using command line.
Working on: git version 2.20.1.windows.1 | Apache Maven 3.6.0 | java se 1.8.0_45-b15

 * $ git clone https://kindzaz@bitbucket.org/kindzaz/github-api-consumer.git;
 * $ cd github-api-consumer;
 * $ mvn clean install;
 * $ mvn test;
 * $ mvn spring-boot:run;
 * open browser, enter http://localhost:8080/java-frameworks?n=100